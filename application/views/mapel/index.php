<div class="card">
  <div class="card-header">
    <h4 class="card-title text-center">
      Data Mata Pelajaran
    </h4>
  </div>
  <div class="card-body">
    <div class="mb-3">
      <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalTambah">
        Tambah
      </button>
    </div>
    <?= $this->session->flashdata('msg') ?>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Mata Pelajaran</th>
          <th scope="col">Menu</th>
        </tr>
      </thead>
      <tbody>
        <?php $n = 1;
        foreach ($mapel as $m) { ?>
          <tr>
            <th scope="row"><?= $n ?></th>
            <td><?= $m['mapelNama'] ?></td>
            <td>
              <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalEdit<?= $m['mapelId'] ?>">Edit</button>
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalHapus<?= $m['mapelId'] ?>">Hapus</button>
            </td>
          </tr>
        <?php $n++;
        } ?>
      </tbody>
    </table>
  </div>
</div>

<!-- tambah -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTambahLabel">Tambah Mata Pelajaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="<?= base_url('admin/mapel/tambah') ?>">
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Mapel</label>
            <input type="text" name="mapelNama" required class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- edit -->
<?php foreach ($mapel as $m) { ?>
  <div class="modal fade" id="modalEdit<?= $m['mapelId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalEdit<?= $m['mapelId'] ?>Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalEdit<?= $m['mapelId'] ?>Label">Edit User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="<?= base_url('admin/mapel/update/' . $m['mapelId']) ?>">
          <div class="modal-body">
            <div class="form-group">
              <label>Nama Mata Pelajaran</label>
              <input type="text" value="<?= $m['mapelNama'] ?>" name="mapelNama" required class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php } ?>

<!-- hapus -->
<?php foreach ($mapel as $m) { ?>
  <div class="modal fade" id="modalHapus<?= $m['mapelId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalHapus<?= $m['mapelId'] ?>Label" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalHapus<?= $m['mapelId'] ?>Label">Hapus User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Hapus Data ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <a href="<?= base_url('admin/mapel/delete/' . $m['mapelId']) ?>" class="btn btn-danger">Hapus</a>
        </div>
      </div>
    </div>
  </div>
<?php } ?>