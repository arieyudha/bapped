<div class="card">
  <div class="card-header">
    <h4 class="card-title text-center">
      Data Kelas
    </h4>
  </div>
  <div class="card-body">
    <div class="mb-3">
      <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalTambah">
        Tambah
      </button>
    </div>
    <?= $this->session->flashdata('msg') ?>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Kelas</th>
          <th scope="col">Menu</th>
        </tr>
      </thead>
      <tbody>
        <?php $n = 1;
        foreach ($kelas as $k) { ?>
          <tr>
            <th scope="row"><?= $n ?></th>
            <td><?= $k['kelasNama'] ?></td>
            <td>
              <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalEdit<?= $k['kelasId'] ?>">Edit</button>
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalHapus<?= $k['kelasId'] ?>">Hapus</button>
            </td>
          </tr>
        <?php $n++;
        } ?>
      </tbody>
    </table>
  </div>
</div>

<!-- tambah -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTambahLabel">Tambah Kelas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="<?= base_url('admin/kelas/tambah') ?>">
        <div class="modal-body">
          <div class="form-group">
            <label>Nama Kelas</label>
            <input type="text" name="kelasNama" required class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- edit -->
<?php foreach ($kelas as $k) { ?>
  <div class="modal fade" id="modalEdit<?= $k['kelasId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalEdit<?= $k['kelasId'] ?>Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalEdit<?= $k['kelasId'] ?>Label">Edit Kelas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="<?= base_url('admin/kelas/update/' . $k['kelasId']) ?>">
          <div class="modal-body">
            <div class="form-group">
              <label>Nama Kelas</label>
              <input type="text" value="<?= $k['kelasNama'] ?>" name="kelasNama" required class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php } ?>

<!-- hapus -->
<?php foreach ($kelas as $k) { ?>
  <div class="modal fade" id="modalHapus<?= $k['kelasId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalHapus<?= $k['kelasId'] ?>Label" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalHapus<?= $k['kelasId'] ?>Label">Hapus User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Hapus Data ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <a href="<?= base_url('admin/kelas/delete/' . $k['kelasId']) ?>" class="btn btn-danger">Hapus</a>
        </div>
      </div>
    </div>
  </div>
<?php } ?>