<div class="card">
  <div class="card-header">
    <h4 class="card-title text-center">
      Data Pengguna
    </h4>
  </div>
  <div class="card-body">
    <?php if ($this->session->userdata('levelId') == 1) { ?>
      <div class="mb-3">
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalTambah">
          Tambah
        </button>
      </div>
    <?php } ?>
    <?= $this->session->flashdata('msg') ?>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama</th>
          <th scope="col">Level</th>
          <?php if ($this->session->userdata('levelId') == 1) { ?>
            <th scope="col">Menu</th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php $n = 1;
        foreach ($user as $u) { ?>
          <tr>
            <th scope="row"><?= $n ?></th>
            <td><?= $u['userNama'] ?></td>
            <td><?= $u['levelNama'] ?></td>
            <?php if ($this->session->userdata('levelId') == 1) { ?>
              <td>
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalEdit<?= $u['userId'] ?>">Edit</button>
                <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalHapus<?= $u['userId'] ?>">Hapus</button>
              </td>
            <?php } ?>
          </tr>
        <?php $n++;
        } ?>
      </tbody>
    </table>
  </div>
</div>


<?php if ($this->session->userdata('levelId') == 1) { ?>
  <!-- tambah -->
  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTambahLabel">Tambah User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="<?= base_url('admin/user/tambah') ?>">
          <div class="modal-body">
            <div class="form-group">
              <label>Level</label>
              <select class="form-control" name="levelId" required>
                <option disabled selected>--PILIH--</option>
                <?php foreach ($level as $l) { ?>
                  <option value="<?= $l['levelId'] ?>"><?= $l['levelNama'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="userNama" required class="form-control">
            </div>
            <div class="form-group">
              <label>Username</label>
              <input type="text" name="userUsername" required class="form-control">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="userPassword" required class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- edit -->
  <?php foreach ($user as $u) { ?>
    <div class="modal fade" id="modalEdit<?= $u['userId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalEdit<?= $u['userId'] ?>Label" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalEdit<?= $u['userId'] ?>Label">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form method="POST" action="<?= base_url('admin/user/update/' . $u['userId']) ?>">
            <div class="modal-body">
              <div class="form-group">
                <label>Level</label>
                <select class="form-control" name="levelId" required>
                  <option disabled>--PILIH--</option>
                  <?php foreach ($level as $l) { ?>
                    <option value="<?= $l['levelId'] ?>" <?= ($l['levelId'] == $u['userId']) ? 'selected' : '' ?>><?= $l['levelNama'] ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label>Nama</label>
                <input type="text" value="<?= $u['userNama'] ?>" name="userNama" required class="form-control">
              </div>
              <div class="form-group">
                <label>Username</label>
                <input type="text" value="<?= $u['userUsername'] ?>" name="userUsername" required class="form-control">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  <?php } ?>

  <!-- hapus -->
  <?php foreach ($user as $u) { ?>
    <div class="modal fade" id="modalHapus<?= $u['userId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalHapus<?= $u['userId'] ?>Label" aria-hidden="true">
      <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalHapus<?= $u['userId'] ?>Label">Hapus User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Hapus Data ?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <a href="<?= base_url('admin/user/delete/' . $u['userId']) ?>" class="btn btn-danger">Hapus</a>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
<?php } ?>