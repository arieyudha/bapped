<div class="card">
  <div class="card-header">
    <h4 class="card-title text-center">
      Data Mata Pelajaran
    </h4>
  </div>
  <div class="card-body">
    <div class="mb-3">
      <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalTambah">
        Tambah
      </button>
    </div>
    <?= $this->session->flashdata('msg') ?>
    <table class="table text-center">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama Guru</th>
          <th scope="col">Kelas</th>
          <th scope="col">Mapel</th>
          <th scope="col">Lama Mengajar (Jam)</th>
          <th scope="col">Menu</th>
        </tr>
      </thead>
      <tbody>
        <?php $n = 1;
        foreach ($pengajar as $p) { ?>
          <tr>
            <th scope="row"><?= $n ?></th>
            <td><?= $p['userNama'] ?></td>
            <td><?= $p['kelasNama'] ?></td>
            <td><?= $p['mapelNama'] ?></td>
            <td><?= $p['lamaMengajar'] ?></td>
            <td>
              <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalEdit<?= $p['detailMapelId'] ?>">Edit</button>
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalHapus<?= $p['detailMapelId'] ?>">Hapus</button>
            </td>
          </tr>
        <?php $n++;
        } ?>
      </tbody>
    </table>
  </div>
</div>

<!-- tambah -->
<div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modalTambahLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTambahLabel">Tambah Pengajar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="<?= base_url('admin/pengajar/tambah') ?>">
        <div class="modal-body">
          <div class="form-group">
            <label>Pengajar</label>
            <select name="userId" required class="form-control">
              <option disabled selected>--PILIH--</option>
              <?php foreach ($guru as $g) { ?>
                <option value="<?= $g['userId'] ?>"><?= $g['userNama'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Kelas</label>
            <select name="kelasId" required class="form-control">
              <option disabled selected>--PILIH--</option>
              <?php foreach ($kelas as $k) { ?>
                <option value="<?= $k['kelasId'] ?>"><?= $k['kelasNama'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Mata Pelajaran</label>
            <select name="mapelId" required class="form-control">
              <option disabled selected>--PILIH--</option>
              <?php foreach ($mapel as $m) { ?>
                <option value="<?= $m['mapelId'] ?>"><?= $m['mapelNama'] ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Lama Pengajar</label>
            <input type="number" name="lamaMengajar" required class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- edit -->
<?php foreach ($pengajar as $p) { ?>
  <div class="modal fade" id="modalEdit<?= $p['detailMapelId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalEdit<?= $p['detailMapelId'] ?>Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalEdit<?= $p['detailMapelId'] ?>Label">Edit Pengajar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="<?= base_url('admin/pengajar/update/' . $p['detailMapelId']) ?>">
          <div class="modal-body">
            <div class="form-group">
              <label>Pengajar</label>
              <select name="userId" required class="form-control">
                <option disabled>--PILIH--</option>
                <?php foreach ($guru as $g) { ?>
                  <option value="<?= $g['userId'] ?>" <?= ($g['userId'] == $p['userId']) ? 'selected' : '' ?>><?= $g['userNama'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Kelas</label>
              <select name="kelasId" required class="form-control">
                <option disabled>--PILIH--</option>
                <?php foreach ($kelas as $k) { ?>
                  <option value="<?= $k['kelasId'] ?>" <?= ($k['kelasId'] == $p['kelasId']) ? 'selected' : '' ?>><?= $k['kelasNama'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Mata Pelajaran</label>
              <select name="mapelId" required class="form-control">
                <option disabled>--PILIH--</option>
                <?php foreach ($mapel as $m) { ?>
                  <option value="<?= $m['mapelId'] ?>" <?= ($m['mapelId'] == $p['mapelId']) ? 'selected' : '' ?>><?= $p['mapelNama'] ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Lama Pengajar</label>
              <input type="number" name="lamaMengajar" value="<?= $p['lamaMengajar'] ?>" class="form-control">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php } ?>

<!-- hapus -->
<?php foreach ($pengajar as $p) { ?>
  <div class="modal fade" id="modalHapus<?= $p['detailMapelId'] ?>" tabindex="-1" role="dialog" aria-labelledby="modalHapus<?= $p['detailMapelId'] ?>Label" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalHapus<?= $p['detailMapelId'] ?>Label">Hapus User</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Hapus Data ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <a href="<?= base_url('admin/pengajar/delete/' . $p['detailMapelId']) ?>" class="btn btn-danger">Hapus</a>
        </div>
      </div>
    </div>
  </div>
<?php } ?>