<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Jekyll v3.8.6">
  <title>Signin Template · Bootstrap</title>

  <link rel="stylesheet" href="<?= base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Testing</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link" href="<?= base_url('admin/user') ?>">User</a>
        <a class="nav-item nav-link" href="<?= base_url('admin/mapel') ?>">Mata Pelajaran</a>
        <a class="nav-item nav-link" href="<?= base_url('admin/kelas') ?>">Kelas</a>
        <a class="nav-item nav-link" href="<?= base_url('admin/pengajar') ?>">Pengajar</a>
        <a class="nav-item nav-link" href="<?= base_url('login/logout') ?>">Logout</a>
      </div>
    </div>
  </nav>
  <div class="container mt-5">
    <?php $this->load->view($page) ?>
  </div>
  <script src="<?= base_url('assets/jquery/jquery-3.4.1.min.js') ?>"></script>
  <script src="<?= base_url('assets/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
</body>

</html>