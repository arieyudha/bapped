<?php

function checkLogin()
{
  $ci = get_instance();
  if ($ci->session->userdata('isLogin') != TRUE) {
    redirect('login');
  }
}

function isLogin()
{
  $ci = get_instance();
  if ($ci->session->userdata('isLogin') == TRUE) {
    redirect('admin/user');
  }
}
