<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_mapel extends CI_Model
{
  public function get()
  {
    return $this->db->from('tb_mapel')->get();
  }

  public function save($ins)
  {
    return $this->db->insert('tb_mapel', $ins);
  }

  public function update($ins, $mapelId)
  {
    return $this->db->update('tb_mapel', $ins, "mapelId= $mapelId");
  }

  public function delete($mapelId)
  {
    return $this->db->delete('tb_mapel', "mapelId= $mapelId");
  }
}
