<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengajar extends CI_Model
{
  public function getWhere($where = null)
  {
    $this->db->from('tb_detail_guru as a')
      ->join('tb_user as b', 'a.userId = b.userId')
      ->join('tb_mapel as c', 'a.mapelId = c.mapelId')
      ->join('tb_kelas as d', 'a.kelasId = d.kelasId');
    if ($where != null) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function save($ins)
  {
    return $this->db->insert('tb_detail_guru', $ins);
  }

  public function update($ins, $detailMapelId)
  {
    return $this->db->update('tb_detail_guru', $ins, "detailMapelId= $detailMapelId");
  }

  public function delete($detailMapelId)
  {
    return $this->db->delete('tb_detail_guru', "detailMapelId= $detailMapelId");
  }
}
