<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kelas extends CI_Model
{
  public function get()
  {
    return $this->db->from('tb_kelas')->get();
  }

  public function save($ins)
  {
    return $this->db->insert('tb_kelas', $ins);
  }

  public function update($ins, $kelasId)
  {
    return $this->db->update('tb_kelas', $ins, "kelasId= $kelasId");
  }

  public function delete($kelasId)
  {
    return $this->db->delete('tb_kelas', "kelasId= $kelasId");
  }
}
