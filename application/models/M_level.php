<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_level extends CI_Model
{
  public function get()
  {
    return $this->db->from('tb_level')->get();
  }
}
