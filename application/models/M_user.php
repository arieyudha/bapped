<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{
  public function getWhere($where = null)
  {
    $this->db->from('tb_user as a');
    $this->db->join('tb_level as b', 'a.levelId = b.levelId', 'left');
    if ($where != null) {
      $this->db->where($where);
    }
    return $this->db->get();
  }

  public function save($ins)
  {
    return $this->db->insert('tb_user', $ins);
  }

  public function update($ins, $userId)
  {
    return $this->db->update('tb_user', $ins, "userId= $userId");
  }

  public function delete($userId)
  {
    return $this->db->delete('tb_user', "userId= $userId");
  }
}
