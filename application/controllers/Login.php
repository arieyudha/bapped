<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_user');
  }
  public function index()
  {
    isLogin();
    $this->load->view('login');
  }

  public function doLogin()
  {
    $login = array(
      'userUsername' => $this->input->post('userUsername', true),
      'userPassword' => $this->input->post('userPassword', true),
    );

    $checkData = $this->M_user->getWhere(array('a.userUsername' => $login['userUsername']));
    $newCheckData = $checkData->row_array();
    if (password_verify($login['userPassword'], $newCheckData['userPassword'])) {
      $setSession = array(
        'isLogin' => TRUE,
        'userId'  => $newCheckData['userId'],
        'levelId'  => $newCheckData['levelId'],
        'userNama'  => $newCheckData['userNama'],
      );
      $this->session->set_userdata($setSession);
      redirect('admin/user');
    } else {
      $this->session->set_flashdata('msg', '
        <div class="alert alert-danger" role="alert">
        Gagal
        </div>
      ');
      redirect('login');
    }
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect('login');
  }
}
