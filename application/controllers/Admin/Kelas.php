<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    checkLogin();
    $this->load->model('M_kelas');
  }
  public function index()
  {
    $data = array(
      'page'  => 'kelas/index',
      'kelas' => $this->M_kelas->get()->result_array()
    );
    $this->load->view('template/dynamic', $data);
  }

  public function tambah()
  {
    $ins = array(
      'kelasNama' => trim($this->input->post('kelasNama')),
    );
    if ($this->M_kelas->save($ins)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/kelas');
  }

  public function update($kelasId)
  {
    $ins = array(
      'kelasNama' => trim($this->input->post('kelasNama')),
    );
    if ($this->M_kelas->update($ins, $kelasId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/kelas');
  }

  public function delete($kelasId)
  {
    if ($this->M_kelas->delete($kelasId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/kelas');
  }
}
