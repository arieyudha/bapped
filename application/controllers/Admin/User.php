<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    checkLogin();
    $this->load->model('M_user');
    $this->load->model('M_level');
  }
  public function index()
  {
    $data = array(
      'page'  => 'user/index',
      'user'  => $this->M_user->getWhere()->result_array(),
      'level'  => $this->M_level->get()->result_array()
    );
    $this->load->view('template/dynamic', $data);
  }

  public function tambah()
  {
    $ins = array(
      'userNama' => trim($this->input->post('userNama')),
      'userUsername' => trim($this->input->post('userUsername')),
      'userPassword' => password_hash($this->input->post('userPassword'), PASSWORD_BCRYPT),
      'levelId' => trim($this->input->post('levelId'))
    );
    if ($this->M_user->save($ins)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/user');
  }

  public function update($userId)
  {
    $ins = array(
      'userNama' => trim($this->input->post('userNama')),
      'userUsername' => trim($this->input->post('userUsername')),
      'levelId' => trim($this->input->post('levelId'))
    );
    if ($this->M_user->update($ins, $userId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/user');
  }

  public function delete($userId)
  {
    if ($this->M_user->delete($userId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/user');
  }
}
