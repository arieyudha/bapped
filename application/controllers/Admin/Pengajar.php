<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajar extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    checkLogin();
    $this->load->model('M_pengajar');
    $this->load->model('M_user');
    $this->load->model('M_kelas');
    $this->load->model('M_mapel');
  }
  public function index()
  {
    $data = array(
      'page'  => 'pengajar/index',
      'pengajar'  => $this->M_pengajar->getWhere(array('b.levelId' => 2))->result_array(),
      'guru'    => $this->M_user->getWhere(array('a.levelId' => 2))->result_array(),
      'kelas'    => $this->M_kelas->get()->result_array(),
      'mapel'    => $this->M_mapel->get()->result_array(),
    );
    $this->load->view('template/dynamic', $data);
  }

  public function tambah()
  {
    $ins = array(
      'userId' => trim($this->input->post('userId')),
      'kelasId' => trim($this->input->post('kelasId')),
      'mapelId' => trim($this->input->post('mapelId')),
      'lamaMengajar' => trim($this->input->post('lamaMengajar'))
    );
    if ($this->M_pengajar->save($ins)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/pengajar');
  }

  public function update($detailMapelId)
  {
    $ins = array(
      'userId' => trim($this->input->post('userId')),
      'kelasId' => trim($this->input->post('kelasId')),
      'mapelId' => trim($this->input->post('mapelId')),
      'lamaMengajar' => trim($this->input->post('lamaMengajar'))
    );
    if ($this->M_pengajar->update($ins, $detailMapelId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/pengajar');
  }

  public function delete($detailMapelId)
  {
    if ($this->M_pengajar->delete($detailMapelId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/pengajar');
  }
}
