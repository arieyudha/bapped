<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapel extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    checkLogin();
    $this->load->model('M_mapel');
  }
  public function index()
  {
    $data = array(
      'page'  => 'mapel/index',
      'mapel' => $this->M_mapel->get()->result_array()
    );
    $this->load->view('template/dynamic', $data);
  }

  public function tambah()
  {
    $ins = array(
      'mapelNama' => trim($this->input->post('mapelNama')),
    );
    if ($this->M_mapel->save($ins)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/mapel');
  }

  public function update($mapelId)
  {
    $ins = array(
      'mapelNama' => trim($this->input->post('mapelNama')),
    );
    if ($this->M_mapel->update($ins, $mapelId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/mapel');
  }

  public function delete($mapelId)
  {
    if ($this->M_mapel->delete($mapelId)) {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-success" role="alert">
        Berhasil
      </div>
      ');
    } else {
      $this->session->set_flashdata('msg', '
      <div class="alert alert-danger" role="alert">
        Gagal
      </div>
      ');
    }
    redirect('admin/mapel');
  }
}
