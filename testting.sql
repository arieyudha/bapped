-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2020 at 07:37 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testting`
--
CREATE DATABASE IF NOT EXISTS `testting` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `testting`;

-- --------------------------------------------------------

--
-- Table structure for table `tb_detail_guru`
--

CREATE TABLE `tb_detail_guru` (
  `detailMapelId` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `mapelId` int(10) UNSIGNED NOT NULL,
  `kelasId` int(10) UNSIGNED NOT NULL,
  `lamaMengajar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `kelasId` int(10) UNSIGNED NOT NULL,
  `kelasNama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`kelasId`, `kelasNama`) VALUES
(1, '7'),
(4, '8');

-- --------------------------------------------------------

--
-- Table structure for table `tb_level`
--

CREATE TABLE `tb_level` (
  `levelId` tinyint(1) UNSIGNED NOT NULL,
  `levelNama` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_level`
--

INSERT INTO `tb_level` (`levelId`, `levelNama`) VALUES
(1, 'Admin'),
(2, 'Guru');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mapel`
--

CREATE TABLE `tb_mapel` (
  `mapelId` int(10) UNSIGNED NOT NULL,
  `mapelNama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_mapel`
--

INSERT INTO `tb_mapel` (`mapelId`, `mapelNama`) VALUES
(1, 'Matematika'),
(4, 'Bahasa Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `userId` int(10) UNSIGNED NOT NULL,
  `userNama` varchar(50) NOT NULL,
  `userUsername` varchar(50) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `levelId` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`userId`, `userNama`, `userUsername`, `userPassword`, `levelId`) VALUES
(8, 'admin', 'admin', '$2y$10$GpLxRnQIE9uJ3qbGJEHX1OjBC5nWGOeUb9nZpgfeDailMUxchqtAi', 1),
(9, 'Mulyadi', 'mulyadi', '$2y$10$SSaL6j6bJmS2vNb0rq0guOTw58EsIUPg7OEIOkrfRm.jleSfgE1pi', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_detail_guru`
--
ALTER TABLE `tb_detail_guru`
  ADD PRIMARY KEY (`detailMapelId`),
  ADD KEY `FK_tb_detail_guru_tb_user` (`userId`),
  ADD KEY `FK_tb_detail_guru_tb_kelas` (`kelasId`),
  ADD KEY `FK_tb_detail_guru_tb_mapel` (`mapelId`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`kelasId`);

--
-- Indexes for table `tb_level`
--
ALTER TABLE `tb_level`
  ADD PRIMARY KEY (`levelId`);

--
-- Indexes for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  ADD PRIMARY KEY (`mapelId`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userUsername` (`userUsername`),
  ADD KEY `FK_tb_users_tb_level` (`levelId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_detail_guru`
--
ALTER TABLE `tb_detail_guru`
  MODIFY `detailMapelId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `kelasId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_level`
--
ALTER TABLE `tb_level`
  MODIFY `levelId` tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_mapel`
--
ALTER TABLE `tb_mapel`
  MODIFY `mapelId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `userId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_detail_guru`
--
ALTER TABLE `tb_detail_guru`
  ADD CONSTRAINT `FK_tb_detail_guru_tb_kelas` FOREIGN KEY (`kelasId`) REFERENCES `tb_kelas` (`kelasId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_tb_detail_guru_tb_mapel` FOREIGN KEY (`mapelId`) REFERENCES `tb_mapel` (`mapelId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_tb_detail_guru_tb_user` FOREIGN KEY (`userId`) REFERENCES `tb_user` (`userId`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `FK_tb_users_tb_level` FOREIGN KEY (`levelId`) REFERENCES `tb_level` (`levelId`) ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
